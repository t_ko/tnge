//#define NDEBUG
#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include "GapBuffer.h"
#include <stack>
#include <assert.h>

using namespace std;

/* konstruktori, jossa gapbufferin oletuskoko on 256 */

GapBuffer::GapBuffer(size_t size = 256) {
        this->size = size;
        start_pointer = new char[size];
        cursor_pointer = start_pointer;
        end_pointer = start_pointer + size;
        gap_end_pointer = end_pointer;
        this->saved = true;
        this->stack_top = 0;
};

/* kopiokonstruktori */

GapBuffer::GapBuffer(const GapBuffer& rhs) {
        this->size = rhs.size;
        start_pointer = new char[size];
        copy(rhs.start_pointer, rhs.end_pointer, start_pointer);
        end_pointer = start_pointer + size;
        gap_end_pointer = start_pointer + (rhs.gap_end_pointer - rhs.start_pointer);
};

/* vaihtaa vasemman ja oikean puolen pointterit keskenään */

void swap(GapBuffer& lhs, GapBuffer& rhs) {
        swap(lhs.size, rhs.size);
        swap(lhs.start_pointer, rhs.start_pointer);
        swap(lhs.cursor_pointer, rhs.cursor_pointer);
        swap(lhs.end_pointer, rhs.end_pointer);
        swap(lhs.gap_end_pointer, rhs.gap_end_pointer);
};
GapBuffer& GapBuffer::operator=(GapBuffer rhs) {
        swap(*this, rhs);
        return *this;
};
int GapBuffer::debug_stack() {
        return this->change_stack.size();
}

/* lisää yhden merkin gapbufferiin */

void GapBuffer::insert(char c) {
        (*cursor_pointer++) = c;
        update_size();
        this->saved = false;
};

/* poistaa viimeisimmän merkin gapbufferista */

void GapBuffer::remove_last_ch() {
        if (cursor_pointer > start_pointer) {   // lisätty tarkistus, että cursor-pointer on suurempi kuin start-pointer
                --cursor_pointer;
        }
        this->saved = false;
};

/* liikuttaa kursoria vasemmalle tai oikealle, kopioiden gapbufferin sisältöä sen mukaisesti */

void GapBuffer::move_cursor(ptrdiff_t a_offset = 1) {
        if (a_offset > 0) {
                copy(gap_end_pointer, gap_end_pointer + a_offset, cursor_pointer);
                gap_end_pointer += a_offset;
                cursor_pointer += a_offset;
        } else if (a_offset < 0) {
                copy(cursor_pointer + a_offset, cursor_pointer, gap_end_pointer + a_offset);
                gap_end_pointer += a_offset;
                cursor_pointer += a_offset;
        }
};

/* suurentaa gapbufferin kokoa, kun se on täysi */

void GapBuffer::update_size() {
        if (cursor_pointer == gap_end_pointer) {
                size *= 2;
                char* tmp_pointer = new char[size];
                copy(start_pointer, cursor_pointer, tmp_pointer);
                copy(gap_end_pointer, end_pointer, tmp_pointer + size - (end_pointer - gap_end_pointer));

                cursor_pointer = tmp_pointer + (cursor_pointer - start_pointer);
                gap_end_pointer = tmp_pointer + size - (end_pointer - gap_end_pointer);
                end_pointer = tmp_pointer + size;

                delete[] start_pointer;
                start_pointer = tmp_pointer;
        }
};



/* muodostaa gapbufferin sisällöstä stringin, jonka palauttaa UI-luokkaan */

string GapBuffer::get_buffer_string() {
        string s = "";
        for (char* i = start_pointer; i < cursor_pointer; ++i) {
                s += *i;
        }
        if (gap_end_pointer < end_pointer) {
                for (char* i = gap_end_pointer; i < end_pointer; ++i) {
                        s += *i;
                }
        }
        return s;
}

/* metodi, jolla tarkistetaan onko viimeisimmät muutokset tallennettu */

bool GapBuffer::changes_saved() {
        return this->saved;
}

/* metodi, jolla tarkistetaan onko muutoksia tapahtunut */

bool GapBuffer::check_changes() {
        if (this->stack_top > 0) { return true; }
        else { return false; }
}

/* metodi, jolla tallennetaan gapbufferin sisältö tiedostoon */

bool GapBuffer::save_buffer_to_file(string f) {
        //string extension = ".tnge";
        //string full_name = f += extension;
        ofstream writefile(f.c_str());
        if (!writefile) {
                cerr << "error! file cannot be accessed." << endl;
                return false;
        }
        writefile << this->get_buffer_string();
        this->saved = true;
        writefile.close();
        return true;
}

/* metodi, jolla luetaan tiedoston sisältö gapbufferiin */

bool GapBuffer::read_from_file_to_buffer(string f) {
        fstream readfile(f.c_str(), fstream::in);
        char ch;
        if (readfile.is_open()) {
                while (readfile >> noskipws >> ch) {
                        this->insert(ch);
                }
                readfile.close();
                return true;
        }
        this->saved = true;
        return false;
}

/* metodi, jolla tallennetaan bufferin tietyn ajanhetken tila stackiin */

void GapBuffer::save_state_to_stack() {
        string s = get_buffer_string();
        ++this->stack_top;
        change_stack.push_back(s);
        undo_committed = false;
}

void GapBuffer::save_state_to_stack_d() {

};

/* metodi, jolla kumotaan viimeisin toiminto / tehdään uudelleen kumottu toiminto */

void GapBuffer::undo_redo_changes() {

        //assert(this->stack_top > 0);

        if ((unsigned)this->stack_top == this->change_stack.size()) { --this->stack_top; }
        this->cursor_pointer = this->start_pointer;
        this->end_pointer = this->start_pointer + size;
        this->gap_end_pointer = this->end_pointer;
        string prev_string = change_stack.at(stack_top - 1);
        for (int i = 0; (unsigned) i < prev_string.size(); ++i) {
                insert(prev_string[i]);
        }
        undo_committed = true;
}

/* metodi, jolla palautetaan gapbufferin kapasiteetti */

size_t GapBuffer::get_buffer_capacity() {
        return this->size;
};
