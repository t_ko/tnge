#ifndef GAPBUFFER_H
#define GAPBUFFER_H

#include <stdio.h>
#include <vector>
using std::string;
class GapBuffer {
        public:
                GapBuffer(size_t size);
                GapBuffer(const GapBuffer& rhs);
                ~GapBuffer() { delete[] start_pointer; };
                friend void swap(GapBuffer& lhs, GapBuffer& rhs);
                GapBuffer& operator=(GapBuffer rhs);
                void insert(char c);
                void remove_last_ch();
                void move_cursor(ptrdiff_t a_offset);
                string get_buffer_string();
                size_t get_buffer_capacity();
                bool changes_saved();
                bool save_buffer_to_file(string f);
                bool read_from_file_to_buffer(string f);
                void undo_redo_changes();
                //void redo_changes();
                void save_state_to_stack();
                bool check_changes();
                int debug_stack();

        private:
                bool saved;
                bool undo_committed;
                char* start_pointer;
                char* cursor_pointer;
                char* gap_end_pointer;
                char* end_pointer;
                char size;
                void update_size();
                void save_state_to_stack_d();
                int stack_top;
                std::vector<string> change_stack;
};

#endif