#include <iostream>
#include <ncurses.h>
#include <string.h>
#include "GapBuffer.h"

#define NUMBER_OF_ELEMENTS(x) (sizeof(x) / sizeof(x[0]))

using namespace std;

WINDOW *create_newwin(int height, int width, int start_y, int start_x, int w_type);
void destroy_win(WINDOW *local_win);

int main() {

        /* ikkuna, johon varsinainen teksti kirjoitetaan */

        WINDOW *text_area;

        /* ikkuna, josta näkyy editorin sillä hetkellä aktiivisena oleva tila */

        WINDOW *status_area;

        /* ikkuna, johon on listattu mahdolliset komennot ja josta myös näkyy aktiivisena oleva tila */

        WINDOW *help_area;

        /* varsinainen tietorakenne, jossa aloituskokona on 256 merkkiä */

        GapBuffer gb = GapBuffer(256);

        /* tallennetaan käyttäjän syöte */

        char input_char;

        /* ikkunan aloituskoordinaatit */

        int win_x = 0, win_y = 0;

        /* määrittää komentomoodin sekä sen, poistutaanko editorista */

        bool cmd_mode = true, exit = false;

        /* tallennetaan gapbufferin sisältö */

        string buffer_contents;

        /* uusi, tallentamaton tiedosto */

        string fname = "new file";

        /* tiedostonimen yhteyteen tulostettava viesti */

        string fname_message;

        /* käynnistetään ncurses-tila */

        initscr();

        /* rivien bufferointi ja merkkien prosessointi pois päältä */

        cbreak();

        /* käyttäjän ei echoteta */

        noecho();

        /* kun tekstieditori on päällä */

        while (!exit) {

                /* tallennetaan chariin status_areasta saatava merkki */

                input_char = wgetch(status_area);

                /* luodaan uudet ikkunat */

                text_area = create_newwin(LINES - 10, COLS, 0, 0, 1);
                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                help_area = create_newwin(10, (COLS / 2) -1, LINES - 10, (COLS / 2), 3);

                /* mahdollistaa text_area-ikkunan vierittämisen */

                scrollok(text_area, true);

                /* mahdollistaa mm. funktionäppäinten lukemisen text_area- ja status_area-ikkunoissa */

                keypad(text_area, true);
                keypad(status_area, true);

                /* siirretään kursoria status_arealla näihin koordinaatteihin */

                wmove(status_area, 2, 2);

                /* lisätään status_arean otsikko */

                wattron(status_area, A_UNDERLINE);
                mvwaddnstr(status_area, 1, 1, "status", -1);
                wmove(status_area, 2, 1);
                wattroff(status_area, A_UNDERLINE);

                /* lisätään help_arean otsikko */
                wattron(help_area, A_UNDERLINE);
                mvwaddnstr(help_area, 1, 1, "commands", -1);
                wattroff(help_area, A_UNDERLINE);

                /* lisätään help_areaan luettelo mahdollisista komennoista */

                mvwaddnstr(help_area, 3, 1, ":i insert mode", -1);
                mvwaddnstr(help_area, 4, 1, ":o open file", -1);
                mvwaddnstr(help_area, 5, 1, ":s save file", -1);
                mvwaddnstr(help_area, 6, 1, ":q exit editor", -1);
                mvwaddnstr(help_area, 7, 1, "<ESC> command mode", -1);
                mvwaddnstr(help_area, 3, 20, ":u undo/redo", -1);
                mvwaddnstr(help_area, 5, 20, "h move cursor left", -1);
                mvwaddnstr(help_area, 6, 20, "l move cursor right", -1);

                /* tnge-logo help_areaan */

                wattron(help_area, A_DIM);
                mvwaddnstr(help_area, 2, COLS / 3, "|", -1);
                mvwaddnstr(help_area, 3, COLS / 3, "|--- ,---.,---.,---.", -1);
                mvwaddnstr(help_area, 4, COLS / 3, "|    |   ||   ||---'", -1);
                mvwaddnstr(help_area, 5, COLS / 3, "`---'`   '`---|`---'", -1);
                mvwaddnstr(help_area, 6, COLS / 3, "          `---' v. 1.0", -1);
                mvwaddnstr(help_area, 7, COLS / 3, "(c) 2013 Iiro & Tapio solutions", -1);
                wattroff(help_area, A_DIM);

                /* päivitetään status_area- ja help_area-ikkunat */

                wrefresh(help_area);
                wrefresh(status_area);

                /* komentotilassa suoritettava koodi */

                if (cmd_mode) {

                        /* tallennetaan tietorakenteen tämän hetken tila stackiin kumoamisen varalle */

                        gb.save_state_to_stack();

                        /* tulostetaan status_areaan tämä teksti */

                        mvwaddnstr(status_area, 3, 1, "command mode", -1);

                        /* tulostetaan tiedostonimi */

                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                        else { fname_message = "file being modified: " + fname + "*"; }
                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                        /* muutetaan help_arean teksti blinkatuksi */

                        wattron(help_area, A_BLINK);
                        mvwaddnstr(help_area, 7, 1, "<ESC> command mode", -1);
                        wattroff(help_area, A_BLINK);

                        /* tulostetaan gapbufferin sisältö text_areaan */

                        buffer_contents = gb.get_buffer_string();
                        mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);

                        /* switch-casessa luetaan syötteen saatava merkki ja toimitaan sen mukaisesti */

                        switch (input_char) {

                                /* nämä asiat tehdään kun ':' on valittu */

                                case (int) ':':

                                        /* tuhotaan status_area ja luodaan uusiksi */

                                        destroy_win(status_area);
                                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);

                                        /* tulostetaan tiedostonimi */

                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                        else { fname_message = "file being modified: " + fname + "*"; }
                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                        /* otsikko alleviivataan */

                                        wattron(status_area, A_UNDERLINE);
                                        mvwaddnstr(status_area, 1, 1, "status", -1);
                                        wattroff(status_area, A_UNDERLINE);

                                        /* lisätään status_areaan, kun nappia painetaan */

                                        mvwaddnstr(status_area, 3, 1, ":", -1);

                                        /* päivitetäänkaikki ikkunat */

                                        wrefresh(help_area);
                                        wrefresh(status_area);
                                        wrefresh(text_area);

                                        /* poistetaan syötetyt merkit puskurista */

                                        flushinp();

                                        /* luetaan jälleen syötteen saatava merkki... */

                                        input_char = wgetch(status_area);

                                        /* ...ja toimitaan sen mukaisesti switch-casessa */

                                        switch (input_char) {

                                                /* tiedoston avaaminen */

                                                case (int) 'o':
                                                        if(buffer_contents == ""){
                                                                destroy_win(status_area);
                                                                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                                if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                                else { fname_message = "file being modified: " + fname + "*"; }
                                                                mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                                wattron(status_area, A_UNDERLINE);
                                                                mvwaddnstr(status_area, 1, 1, "status", -1);
                                                                wattroff(status_area, A_UNDERLINE);

                                                                mvwaddnstr(status_area, 3, 1, "write file name to open: ", -1);

                                                                /* Tulostetaan kirjoitetut merkit */

                                                                echo();

                                                                /* Luetaan käyttäjän syöte ja luetaan syötteen mukainen tiedosto bufferiin
                                                                   (jos löytyy) */

                                                                wgetstr(status_area, (char*)fname.c_str());
                                                                gb.read_from_file_to_buffer(fname);
                                                                flushinp();

                                                                buffer_contents = gb.get_buffer_string();
                                                                mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);
                                                        }

                                                        else {

                                                                destroy_win(status_area);
                                                                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                                if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                                else { fname_message = "file being modified: " + fname + "*"; }
                                                                mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                                wattron(status_area, A_UNDERLINE);
                                                                mvwaddnstr(status_area, 1, 1, "status", -1);
                                                                wattroff(status_area, A_UNDERLINE);

                                                                mvwaddnstr(status_area, 3, 1, "unsaved changes, save (y) ", -1);

                                                                switch(input_char){
                                                                        case (int) 'y':

                                                                                destroy_win(status_area);
                                                                                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                                                if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                                                else { fname_message = "file being modified: " + fname + "*"; }
                                                                                mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                                                wattron(status_area, A_UNDERLINE);
                                                                                mvwaddnstr(status_area, 1, 1, "status", -1);
                                                                                wattroff(status_area, A_UNDERLINE);

                                                                                mvwaddnstr(status_area, 3, 1, "write file name to save: ", -1);

                                                                                echo();
                                                                                wgetstr(status_area, (char*)fname.c_str());
                                                                                gb.save_buffer_to_file(fname);
                                                                                mvwaddnstr(status_area, 4, 1, "file saved!", -1);
                                                                                flushinp();

                                                                                break;
                                                                        default:

                                                                                destroy_win(status_area);
                                                                                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                                                if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                                                else { fname_message = "file being modified: " + fname + "*"; }
                                                                                mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                                                wattron(status_area, A_UNDERLINE);
                                                                                mvwaddnstr(status_area, 1, 1, "status", -1);
                                                                                wattroff(status_area, A_UNDERLINE);

                                                                                mvwaddnstr(status_area, 3, 1, "write file name to open: ", -1);

                                                                                /* Tulostetaan kirjoitetut merkit */

                                                                                echo();

                                                                                /* Luetaan käyttäjän syöte ja luetaan syötteen mukainen tiedosto bufferiin
                                                                                   (jos löytyy) */

                                                                                wgetstr(status_area, (char*)fname.c_str());
                                                                                gb.read_from_file_to_buffer(fname);
                                                                                flushinp();

                                                                                buffer_contents = gb.get_buffer_string();
                                                                                mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);
                                                                                break;
                                                                }

                                                                /* Tulostetaan kirjoitetut merkit */

                                                                echo();

                                                                /* Luetaan käyttäjän syöte ja luetaan syötteen mukainen tiedosto bufferiin
                                                                   (jos löytyy) */

                                                                wgetstr(status_area, (char*)fname.c_str());
                                                                gb.read_from_file_to_buffer(fname);
                                                                flushinp();

                                                                buffer_contents = gb.get_buffer_string();
                                                                mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);

                                                        }
                                                        break;

                                                        /* tiedoston tallentaminen */

                                                case (int) 's':

                                                        destroy_win(status_area);
                                                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                        else { fname_message = "file being modified: " + fname + "*"; }
                                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                        wattron(status_area, A_UNDERLINE);
                                                        mvwaddnstr(status_area, 1, 1, "status", -1);
                                                        wattroff(status_area, A_UNDERLINE);

                                                        mvwaddnstr(status_area, 3, 1, "write file name to save: ", -1);

                                                        echo();
                                                        wgetstr(status_area, (char*)fname.c_str());
                                                        if (NUMBER_OF_ELEMENTS(fname) > 0) {
                                                                gb.save_buffer_to_file(fname);
                                                                mvwaddnstr(status_area, 4, 1, "file saved!", -1);
                                                                flushinp();
                                                        }

                                                        break;

                                                        /* kirjoitustilaan siirtyminen */

                                                case (int) 'i':

                                                        cmd_mode = false;

                                                        destroy_win(status_area);
                                                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                        else { fname_message = "file being modified: " + fname + "*"; }
                                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                                                        wattron(status_area, A_UNDERLINE);
                                                        mvwaddnstr(status_area, 1, 1, "status", -1);
                                                        wattroff(status_area, A_UNDERLINE);

                                                        mvwaddnstr(status_area, 3, 1, "insert mode", -1);
                                                        mvwaddnstr(status_area, 4, 1, "press ESCAPE KEY to switch back", -1);

                                                        wattron(help_area, A_BLINK);
                                                        mvwaddnstr(help_area, 3, 1, ":i insert mode", -1);
                                                        wattroff(help_area, A_BLINK);

                                                        mvwaddnstr(help_area, 7, 1, "<ESC> command mode", -1);

                                                        break;

                                                        /* editorista poistuminen */

                                                case (int) 'q':

                                                        destroy_win(status_area);
                                                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);

                                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                        else { fname_message = "file being modified: " + fname + "*"; }
                                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);
                                                        wattron(status_area, A_UNDERLINE);
                                                        mvwaddnstr(status_area, 1, 1, "status", -1);
                                                        wattroff(status_area, A_UNDERLINE);

                                                        if (gb.changes_saved()) {
                                                                mvwaddnstr(status_area, 3, 1, "type 'y' to quit or any other key to cancel: ", -1);
                                                        }

                                                        else {
                                                                wattron(status_area, A_BOLD);
                                                                mvwaddnstr(status_area, 3, 1, "you have unsaved changes, are you sure you want to quit? (y = yes): ", -1);
                                                                wattroff(status_area, A_BOLD);
                                                        }

                                                        flushinp();
                                                        input_char = wgetch(status_area);
                                                        switch (input_char) {
                                                                case (int) 'y':
                                                                        exit = true;

                                                                        break;
                                                                case ERR:
                                                                        break;
                                                                default:

                                                                        break;
                                                        }
                                                        break;

                                                        /* viimeisimmän toiminnon kumoaminen/kumotun toiminnon uudelleen tekeminen */

                                                case (int) 'u':
                                                        if (gb.check_changes()) {
                                                                destroy_win(status_area);
                                                                status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);

                                                                if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                                else { fname_message = "file being modified: " + fname + "*"; }

                                                                mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);
                                                                mvwaddnstr(status_area, 3, 1, "undo/redo", -1);
                                                                wrefresh(status_area);

                                                                gb.undo_redo_changes();
                                                                destroy_win(text_area);
                                                                text_area = create_newwin(LINES - 10, COLS, 0, 0, 1);
                                                                buffer_contents = gb.get_buffer_string();
                                                                mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);
                                                                wrefresh(text_area);
                                                        }
                                                        break;

                                                        /* virhetila */

                                                case ERR:

                                                        wrefresh(status_area);
                                                        wrefresh(help_area);
                                                        wrefresh(text_area);

                                                        doupdate();
                                                        break;

                                                        /* oletus */

                                                default:

                                                        mvwaddnstr(status_area, 5, 1, "unknown command", -1);
                                                        mvwaddnstr(status_area, 6, 1, "(i)nsert, (o)pen, (s)ave or (q)uit, (n)ew file, (u)ndo, (r)edo", -1);

                                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                                        else { fname_message = "file being modified: " + fname + "*"; }

                                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);
                                                        break;
                                        }
                                        break;

                                        /* navigointi vasemmalle */

                                case (int) 'h':
                                        gb.move_cursor(-1);
                                        break;

                                        /* navigointi oikealle */

                                case (int) 'l':
                                        gb.move_cursor(1);
                                        break;

                                        /* poista merkki kursorin kohdalta */

                                case (int) 'x':
                                        gb.remove_last_ch();
                                        break;

                                        /* virhetila */

                                case ERR:
                                        break;
                        }

                        /* p▒ivitet▒▒n ikkunat */

                        wrefresh(help_area);
                        wrefresh(status_area);
                        wrefresh(text_area);
                }

                /* kirjoitustilassa suoritettava koodi */

                else {
                        wattron(help_area, A_BLINK);
                        mvwaddnstr(help_area, 3, 1, ":i insert mode", -1);
                        wattroff(help_area, A_BLINK);

                        destroy_win(status_area);
                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);

                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                        else { fname_message = "file being modified: " + fname + "*"; }

                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);

                        wattron(status_area, A_UNDERLINE);
                        mvwaddnstr(status_area, 1, 1, "status", -1);
                        wattroff(status_area, A_UNDERLINE);

                        mvwaddnstr(status_area, 3, 1, "insert mode", -1);
                        mvwaddnstr(status_area, 4, 1, "press ESCAPE KEY to switch back", -1);

                        switch (input_char) {

                                /* backspace */

                                case (127):

                                        gb.remove_last_ch();
                                        mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);
                                        destroy_win(text_area);
                                        text_area = create_newwin(LINES - 10, COLS, 0, 0, 1);
                                        wrefresh(text_area);
                                        break;

                                        /* escape */

                                case (27):

                                        destroy_win(status_area);
                                        status_area = create_newwin(10, (COLS / 2), LINES - 10, 0, 2);
                                        wattron(status_area, A_UNDERLINE);
                                        mvwaddnstr(status_area, 1, 1, "status", -1);
                                        wattroff(status_area, A_UNDERLINE);
                                        mvwaddnstr(status_area, 3, 1, "command mode", -1);

                                        if (gb.changes_saved()) { fname_message = "file being modified: " + fname; }
                                        else { fname_message = "file being modified: " + fname + "*"; }

                                        mvwaddnstr(status_area, 1, 30, fname_message.c_str(), -1);
                                        mvwaddnstr(help_area, 3, 1, ":i insert mode", -1);
                                        wattron(help_area, A_BLINK);
                                        mvwaddnstr(help_area, 7, 1, "<ESC> command mode", -1);
                                        wattroff(help_area, A_BLINK);

                                        cmd_mode = true;
                                        break;

                                        /* oletus */

                                default:

                                        /* lisätään merkki gapbufferiin */

                                        gb.insert(input_char);
                                        break;
                        }
                        buffer_contents = gb.get_buffer_string();
                        if (win_x >= COLS-2) { ++win_y; win_x = 2; }
                        mvwaddnstr(text_area, win_y, win_x, buffer_contents.c_str(), -1);
                        wrefresh(help_area);
                        wrefresh(status_area);
                        wrefresh(text_area);
                }
        }

        /* poistutaan ncurses-tilasta */

        endwin();
        return 0;
}

/* metodi, jolla luodaan ikkuna */

WINDOW *create_newwin(int height, int width, int start_y, int start_x, int w_type) {

        WINDOW *local_win;
        local_win = newwin(height, width, start_y, start_x);
        if (w_type < 2) { wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' '); }
        else { whline(local_win, 0, COLS);  }
        wrefresh(local_win);
        return local_win;

}

/* metodi, jolla tuhotaan ikkuna */

void destroy_win(WINDOW *local_win) {

        wborder(local_win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
        wrefresh(local_win);
        delwin(local_win);

}
